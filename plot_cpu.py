import matplotlib.pyplot as plt
import numpy as np

plt.rcParams["font.family"] = "Times New Roman"

thresholds = [
    0,
    20,
    40,
    60,
    80,
    100,
]
total_cpu = 22

def read_file(file_name):
    f = open(file_name, "r")
    cpu_used_list = []
    total = 0
    for line in f:
        if "cpu used:" in line:
            split1 = line.split("cpu used:")[1].strip()
            cpu_used = float(split1.split("total")[0].strip())
            cpu_used_list.append(cpu_used/total_cpu)
            total += cpu_used/total_cpu
    average = total / len(cpu_used_list)
    print("file = " + file_name)
    print("average cpu used = " + str(average))
    f.close()
    return cpu_used_list, average

cpu_data_list = []
ave_cpu_list = []
for i in range(len(thresholds)):
    file_name = "tera_cpu/{}.log".format(thresholds[i])
    cpu_used_list, ave = read_file(file_name)
    cpu_data_list.append(cpu_used_list)
    ave_cpu_list.append(ave)


fig = plt.figure()
fig.subplots_adjust(hspace=1.75)


for i in range(len(thresholds)):
    ax = plt.subplot(len(thresholds), 1, i+1)
    ax.set_title("Supernode Threshold: {}".format(thresholds[i]))
    t = np.arange(0, len(cpu_data_list[i]), 1)
    ax.hlines(ave_cpu_list[i], t[0], t[-1], colors='r', linestyle='dashed')
    ax.text(t[0], ave_cpu_list[i], str(round(ave_cpu_list[i],2)), color='r', ha='right', va='center')
    ax.plot(t, cpu_data_list[i])

fig2 = plt.figure()
ax = plt.subplot(1, 1, 1)
ax.plot(thresholds, ave_cpu_list)
plt.show()


