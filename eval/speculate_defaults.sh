#!/bin/bash
if [[ ! -n "$SPECULATOR_CLASS" ]]; then
    SPECULATOR_CLASS="DefaultSpeculator"
fi

if [[ ! -n "$SUPERNODE_THRESHOLD" ]]; then
    SUPERNODE_THRESHOLD=70
fi