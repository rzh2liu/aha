#!/bin/bash
cd ../setup
source ./shared.sh

#
# Load simulation based on 10 minute segments of the hour
#
# Load pattern example:
#|   0-10    |   10-20   |   20-30   |   30-40   |   40-50   |   50-60  |  -> repeat from 0-10
#   load A      load B      load A      load B      load C      load C
#
# load A: even datanodes stressed
# load B: odd datanodes stressed
# load C: all idle

# CYCLE is in minutes
CYCLE=10
T=$((CYCLE*60))
declare -i k=0
F=2
while :; do
  minute=$(date +"%-M")
  # every 10 minutes we change load type
  if [[ $((minute % $CYCLE)) == 0 ]]; then
    minute_tens=$(bc <<< "$minute / 10")
    if [[ ${minute_tens} -eq 4 ]] || [[ ${minute_tens} -eq 5 ]]; then
        echo "dnodes idle for ${T}"
    else
        # switch between even/odd dnodes to load every 10 minutes
        load_enabled=$((minute_tens % 2))
        declare -i i=0
        for DATANODE_IP in "${DATANODE_SSH_IPS[@]}"
        do
        if [[ $((i % 2)) == $load_enabled ]]; then
            CPU=$(bc <<< "${DATANODE_VCORES[$i]} * 60/100")
            # check at least 1 vcore
            if [[ ${CPU} < 1 ]]; then
                CPU=1
            fi
            MEM=$(bc <<< "${DATANODE_MEMS[$i]} * 1000 * 50/100")
            echo "dnode$i stress for ${T} seconds - CPU: ${CPU}vcores MEM: ${MEM}mb"
            ssh -i $SSH_KEY $SSH_USER@$DATANODE_IP screen -d -m "stress --cpu ${CPU} --vm 1 --vm-bytes ${MEM}M --timeout ${T}"
        else
            echo "dnode$i idle for ${T} seconds"
        fi
        i=$((i+1))
        done
    fi 
    echo ""
    sleep ${T}
  else
    sleep 1
  fi
done