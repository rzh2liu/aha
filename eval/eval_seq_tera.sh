#!/bin/bash
NUM_JOBS=$1
JOB_SIZE=$2

source ./speculate_defaults.sh

if [[ ! -n "$NUM_JOBS" ]] || [[ ! -n "$JOB_SIZE" ]]; then
    echo ""
    echo "Usage: ./eval_tera.sh <number of jobs> <job size GB> "
    echo ""
    exit
fi

cd ../setup
source ./shared.sh
cd ../eval


echo "--- Evaluating TeraGen/Sort/Validate Sequence (Jobs $NUM_JOBS | Job Size $JOB_SIZE GB )"

function run_tera() {
    NUM_GB=$1
    ID=$2
    SPEC=$3
    THRESH=$4
    GEN_OUT="$HDFS_ROOT/TeraGen-GB-$ID"
    SORT_OUT="$HDFS_ROOT/TeraSort-GB-$ID"
    VALIDATE_OUT="$HDFS_ROOT/TeraValidate-GB-$ID"
    ./run_teragen.sh $NUM_GB $GEN_OUT $SPEC $THRESH noprint
    ./run_terasort.sh $GEN_OUT $SORT_OUT $SPEC $THRESH noprint
    ./run_teravalidate.sh $SORT_OUT $VALIDATE_OUT $SPEC $THRESH noprint
}

function run_eval() {
    echo "--- Setting up HDFS"
    remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -rm -r -skipTrash $HDFS_ROOT/Tera*"
    remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -mkdir -p $HDFS_ROOT"
    echo "--- Kill Outstanding Jobs"
    remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/mapred job -list | grep job_ | awk ' { system("mapred job -kill " $1) } '"
    sleep 5
    clear_log
    spec=$1
    thresh=$2
    for i in $( seq 1 $NUM_JOBS )
    do
        time run_tera $JOB_SIZE $i $spec $thresh
    done
    save_cpu $2
}

function save_cpu() {
    remote_exec $NAMENODE_SSH_IP "cat $HADOOP_HOME/logs/hadoop-ubuntu-resourcemanager-master.log | grep 'cpu' > ~/tera_cpu/$1.log"
}

function clear_log() {
    remote_exec $NAMENODE_SSH_IP "> $HADOOP_HOME/logs/hadoop-ubuntu-resourcemanager-master.log"
}

function run_tera_sequence() {
    # sleep 900
    # run_eval RACSpeculator -1
    # run_eval RACSpeculator 0
    # sleep 180
    # run_eval RACSpeculator 20
    # sleep 180
    # run_eval RACSpeculator 40
    # sleep 180
    # run_eval RACSpeculator 60
    # sleep 180
    run_eval RACSpeculator 80
    # sleep 180
    # run_eval RACSpeculator 100
}
run_tera_sequence

# auto shutdown gcp commands
# cd ../setup
# ./stop_cluster.sh
# cd ./gcp
# ./stop_gcp.sh
