#!/bin/bash
ID_PREFIX="application_1612998003286_00"

cd ../setup
source ./shared.sh
cd ../eval

start=46
end=90
function get_specs() {
     
    for i in {16..18}
    do
        curr_id=$ID_PREFIX$i
        if [[ "$i" -lt "10" ]] 
        then
            curr_id=$ID_PREFIX"0"$i
        fi
        echo "---" $curr_id
        remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/yarn logs -applicationId $curr_id| grep 'launched'"
        echo ""
    done
}

get_specs