#!/bin/bash
#
# NOTE: this script should be executed locally on remote server
#
NAMENODE_IP=$1
DATANODE_VCORES=$2
DATANODE_MEM_GB=$3

HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop
HADOOP_HOME=/usr/local/hadoop

echo "--- Configuring HDFS"
# sed -i "s/<num_datanodes>/$NUM_DATANODES/" ~/config/hdfs-site-datanode.xml
sudo cp ~/config/hdfs-site-datanode.xml $HADOOP_CONF_DIR/hdfs-site.xml
sudo mkdir -p $HADOOP_HOME/data/hdfs/datanode

echo "--- Configuring MapReduce"
cp ~/config/mapred-site-datanode.xml ~/config/mapred-site-datanode-tmp.xml
sed -i "s/<nnode>/$NAMENODE_IP/" ~/config/mapred-site-datanode-tmp.xml
sudo mv ~/config/mapred-site-datanode-tmp.xml $HADOOP_CONF_DIR/mapred-site.xml

sudo chown -R ubuntu $HADOOP_HOME