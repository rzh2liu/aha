#!/bin/bash
source ./gcp_config

function start_vm() {
	gcloud compute instances start $1 --zone $2
}

start_vm $NNODE_NAME $ZONE &
for name in ${DNODE_NAMES[@]}
do
    start_vm $name $ZONE &
done
wait
