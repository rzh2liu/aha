#!/bin/bash
source ./shared.sh

echo "--- Starting Hadoop Cluster"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "export PDSH_RCMD_TYPE=ssh; $HADOOP_HOME/sbin/start-dfs.sh"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "export PDSH_RCMD_TYPE=ssh; $HADOOP_HOME/sbin/start-yarn.sh"
