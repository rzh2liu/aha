#!/bin/bash
#
# NOTE: this should should be run locally on local machine
#
if [[ ! -n $SSH_USER ]]; then
    source ./shared.sh $1
fi


function setup_ssh() {
    # run ssh setup
    echo "--- Setting up SSH"
    
    # gen ssh key on namenode
    if ssh -T -oStrictHostKeyChecking=no -q -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP test -f "$REMOTE_HOME/.ssh/id_rsa.pub"; then
        echo "--- Already created SSH Key"
    else
        echo "--- Creating SSH Key"
        remote_exec $NAMENODE_SSH_IP "rm $REMOTE_HOME/.ssh/id_rsa"
        remote_exec $NAMENODE_SSH_IP "rm $REMOTE_HOME/.ssh/id_rsa.pub"
        for DATANODE_IP in "${DATANODE_SSH_IPS[@]}"
        do
            remote_exec $DATANODE_IP "rm $REMOTE_HOME/.ssh/id_rsa.pub"
        done
        remote_exec $NAMENODE_SSH_IP "ssh-keygen -f $REMOTE_HOME/.ssh/id_rsa -N ''"
        remote_exec $NAMENODE_SSH_IP "cat $REMOTE_HOME/.ssh/id_rsa.pub >> $REMOTE_HOME/.ssh/authorized_keys"
    fi
    # copy key from remote
    remote_to_local $NAMENODE_SSH_IP $REMOTE_HOME/.ssh/id_rsa.pub ./id_rsa.pub

    KEY_CONTENT=$(cat ./id_rsa.pub)
    for DATANODE_IP in "${DATANODE_SSH_IPS[@]}"
    do 
        if ssh -T -oStrictHostKeyChecking=no -q -i $SSH_KEY $SSH_USER@$DATANODE_IP test -f "$REMOTE_HOME/.ssh/id_rsa.pub"; then            
            echo "--- Already added to authorized_keys"
        else
            local_to_remote ./id_rsa.pub $DATANODE_IP $REMOTE_HOME/.ssh/id_rsa.pub 
            cat ./id_rsa.pub | remote_exec $DATANODE_IP "cat >> $REMOTE_HOME/.ssh/authorized_keys"
        fi
    done

    echo "--- Setting up SSH Config"
    # create local ssh config file
    SSH_CONFIG_FILE="ssh_config"
    echo "Host nnode
  HostName $NAMENODE_CONFIG_IP
  User $SSH_USER  
  IdentityFile $REMOTE_HOME/.ssh/id_rsa" >> $SSH_CONFIG_FILE
    declare -i i=1
    for DATANODE_IP in "${DATANODE_CONFIG_IPS[@]}"
    do
        echo "Host dnode$i
  HostName $DATANODE_IP
  User $SSH_USER 
  IdentityFile $REMOTE_HOME/.ssh/id_rsa" >> $SSH_CONFIG_FILE
        ((i=i+1))
    done
    # transfer to namenode
    local_to_remote $SSH_CONFIG_FILE $NAMENODE_SSH_IP $REMOTE_HOME/.ssh/config
    remote_exec $NAMENODE_SSH_IP "rm -rf $REMOTE_HOME/.ssh/known_hosts"

    # clean up
    rm $SSH_CONFIG_FILE
    rm ./id_rsa.pub
    echo ""
}

setup_ssh

echo "--- SSH Test"
for j in $( seq 1 $NUM_DATANODES )
do
    echo "--- Testing dnode$j"
    remote_exec $NAMENODE_SSH_IP "ssh -oStrictHostKeyChecking=no dnode$j echo OK "
done
echo "--- Testing nnode"
remote_exec $NAMENODE_SSH_IP "ssh -oStrictHostKeyChecking=no nnode echo OK "

echo "Done"
echo ""