#!/bin/bash
source ./shared.sh

echo "--- Run word count job"
scp -o StrictHostKeyChecking=no -r -i $SSH_KEY ./wordcount $SSH_USER@$NAMENODE_SSH_IP:~/wc
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "echo $HADOOP_HOME"
echo "--- Building"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "cd ~/wc; $HADOOP_HOME/bin/hadoop com.sun.tools.javac.Main WordCount.java"
echo "--- Jarring"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "cd ~/wc; jar cf wc.jar WordCount*.class"
echo "--- Uploading files to HDFS"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -rm -R /wc_input"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -rm -R /wc_output"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -mkdir /wc_input"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -put ~/wc/file0.txt /wc_input/file0"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -put ~/wc/file1.txt /wc_input/file1"
echo "--- Running Job"
time ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "cd ~/wc; $HADOOP_HOME/bin/hadoop jar wc.jar WordCount /wc_input /wc_output"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -ls /wc_output"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -cat /wc_output/*"
echo "--- Clean up"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "rm -rf ~/wc"
echo "Runtime: $((end-start))"
echo "Done!"

