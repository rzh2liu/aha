#!/bin/bash
echo "--- Building hadoop"
cd ../hadoop
mvn clean install -Pdist -DskipTests -Dtar -Dmaven.javadoc.skip=true -DskipShade -e
cd ../setup