#!/bin/bash
#
# NOTE: this should should be run locally on local machine
#
FILE=./ssh_credentials
if [ ! -f "$FILE" ]; then
    echo ""
    echo "$FILE does not exist."
    echo ""
    exit
fi

if [ ! -f "./config/namenodes" ]; then
    echo ""
    echo "./config/namenodes does not exist."
    echo ""
    exit
fi

if [ ! -f "./config/datanodes" ]; then
    echo ""
    echo "./config/datanodes does not exist."
    echo ""
    exit
fi

NO_PRINT=$1

if [ ! -n "$1" ]; then
    echo "############################################################################################"
fi
IFS=' ' read -r -a SSH_CONFIG <<< $(head -n 1 ./ssh_credentials)
SSH_KEY="${SSH_CONFIG[0]}"
SSH_USER="${SSH_CONFIG[1]}"
if [ ! -n "$1" ]; then
    echo "SSH_KEY: $SSH_KEY | SSH_USER: $SSH_USER"
    echo ""
fi

# read first line of file and then split line by space
IFS=' ' read -r -a NAMENODE_IPS <<< $(head -n 1 ./config/namenodes)
NAMENODE_SSH_IP="${NAMENODE_IPS[0]}"
NAMENODE_CONFIG_IP="${NAMENODE_IPS[1]}"
NAMENODE_VCORE="${NAMENODE_IPS[2]}"
NAMENODE_MEM="${NAMENODE_IPS[3]}"
if [ ! -n "$1" ]; then
    echo "NAMENODE_SSH_IP: $NAMENODE_SSH_IP | NAMENODE_CONFIG_IP: $NAMENODE_CONFIG_IP | VCORE: $NAMENODE_VCORE | MEM: $NAMENODE_MEM"  
    echo ""
fi

# count number of lines in datanodes
NUM_DATANODES=$(awk 'END{print NR}' ./config/datanodes)
if [ ! -n "$1" ]; then
    echo "# DATANODES: $NUM_DATANODES"
    echo ""
fi

# load datanode ips
DATANODE_CONFIG_IPS=()
DATANODE_SSH_IPS=()
DATANODE_VCORES=()
DATANODE_MEMS=()
while IFS= read -r line || [[ -n $line ]]; do
    IFS=' ' read -r -a DATANODE_IPS <<< $line
    DATANODE_SSH_IPS+=("${DATANODE_IPS[0]}")
    DATANODE_CONFIG_IPS+=("${DATANODE_IPS[1]}")
    DATANODE_VCORES+=("${DATANODE_IPS[2]}")
    DATANODE_MEMS+=("${DATANODE_IPS[3]}")
    if [ ! -n "$1" ]; then
        echo "DATANODE_SSH_IP: ${DATANODE_IPS[0]} | DATANODE_CONFIG_IP: ${DATANODE_IPS[1]} | VCORE: ${DATANODE_IPS[2]} | MEM: ${DATANODE_IPS[3]}"  
    fi
done < ./config/datanodes
if [ ! -n "$1" ]; then
    echo "############################################################################################"
fi

HDFS_ROOT="/user/hdfs"
REMOTE_HOME=/home/$SSH_USER
HADOOP_HOME=/usr/local/hadoop
HADOOP_EXAMPLES=/usr/local/hadoop/share/hadoop/mapreduce


function remote_to_local() {
    # copy file from remote to local via scp
    TARGET_IP=$1
    SOURCE_PATH=$2
    TARGET_PATH=$3
    echo "--- Copy $TARGET_IP:$SOURCE_PATH -> $TARGET_PATH"
    scp -r -i $SSH_KEY $SSH_USER@$TARGET_IP:$SOURCE_PATH $TARGET_PATH
}

function local_to_remote() {
    # copy file from local to remote via scp
    SOURCE_PATH=$1
    TARGET_IP=$2
    TARGET_PATH=$3
    echo "--- Copy $SOURCE_PATH -> $TARGET_IP:$TARGET_PATH"
    scp -r -i $SSH_KEY $SOURCE_PATH $SSH_USER@$TARGET_IP:$TARGET_PATH
}

function remote_exec() {
    # execute command on target ip machine using ssh
    TARGET_IP=$1
    shift
    CMD=$@
    ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$TARGET_IP $CMD
}