#!/bin/bash
echo "--- Installing jFuzzyLogic"
mvn install:install-file -Dfile=./jFuzzyLogic.jar -DgroupId=net.sourceforge.jFuzzyLogic -DartifactId=jFuzzyLogic -Dversion=1.0 -Dpackaging=jar -DgeneratePom=true
