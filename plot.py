import matplotlib.pyplot as plt

def string_to_seconds(s):
    t_str = s.split("m")
    # print(float(t_str[0])*60 + float(t_str[1].split("s")[0]))
    return float(t_str[0])*60 + float(t_str[1].split("s")[0])

def average(data):
    averages = []
    for l in data:
        result = 0
        for t_str in l:
            # print(string_to_seconds(t_str))
            result = result + string_to_seconds(t_str)
        averages.append(result/len(l))
    return averages

default = [
    [
        "3m31.467s",
        "3m6.232s",
        "3m23.936s",
    ],
    [
        "7m35.273s",
        "7m48.336s",
        "7m44.006s",
    ],
    [   
        "18m14.788s",
        "18m47.295s",
        "18m5.049s",
    ],
    [
        "63m19.622s",
        "43m34.936s",
        "60m51.304s",
        "41m4.190s",
    ],
]

hc = [
    [
        "3m19.443s",
        "3m18.371s",
        "3m43.306s",
    ],
    [
        "7m52.100s",
        "7m9.328s",
        "7m36.500s",
    ],
    [
        "16m50.545s",
        "16m32.404s",
        "17m31.549s",
    ],
    [
        "46m14.980s",
        "27m3.882s",
        "44m34.587s",
        "64m43.962s",
    ],
]


wcdefault = [
    [
        "1m55.895s",
        "1m57.103s",
        "1m49.708s",
    ],
    [
        "4m50.576s",
        "5m1.049s",
        "5m5.129s",
    ],
    [   
        "8m38.526s",
        "8m43.376s",
        "8m15.534s",
    ],
    [
        "21m19.795s",
        "21m30.754s",
        # "60m51.304s",
        # "41m4.190s",
    ],
]

wchc = [
    [
        "1m52.945s",
        "1m46.202s",
        "1m52.886s",
    ],
    [
        "4m45.148s",
        "5m14.907s",
        "5m7.870s",
    ],
    [
        "7m52.334s",
        "8m12.193s",
        "8m14.056s",
    ],
    [
        "22m2.135s",
        "21m19.159s",
        # "44m34.587s",
        # "64m43.962s",
    ],
]


# wcdefault = [
#     ["1m47.797s", "2m36.306s"],
#     ["4m22.251s", "4m40.198s"],
#     ["8m18.428s", "7m52.758s"],
#     ["22m17.923s", "22m15.172s"]
# ]

# wchc = [
#     ["1m57.235s", "2m30.552s"],
#     ["4m29.688s", "4m31.141s"],
#     ["8m6.670s", "7m58.006s"],
#     ["21m46.125s", "23m57.889s"]
# ]

datasize = [
    1,
    5,
    10,
    30,
]

default = average(default)
hc = average(hc)

wcdefault = average(wcdefault)
wchc = average(wchc)

plt.title('TeraSort Results')
plt.plot(datasize, default, label="Default", linestyle='-', marker='o',)
plt.plot(datasize, hc, label="History-Considering", linestyle='-', marker='x',)
# plt.plot(datasize, wcdefault, label="Default", linestyle='-', marker='o',)
# plt.plot(datasize, wchc, label="History-Considering", linestyle='-', marker='x',)
plt.ylabel('Job Execution Time (seconds)')
plt.xlabel('Input Size (GB)')
plt.legend()
plt.show()